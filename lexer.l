%option noyywrap
%option yylineno
%{
#include "string.h"
#include "tokens.h"
%}


ID       ("_"([A-Za-z])+[0-9a-zA-Z]*)|([A-Za-z])[a-zA-Z0-9"_"]*
%%
[0-9]+ { yylval.char_t = strdup(yytext);return TK_NUMBER; }
"+"    { return OP_ADD; }
"-"    { return OP_MINUS; }
"*"    { return OP_MUL; }
"/"    { return OP_DIV; }
"("    { return OP_LEFT_PAR; }
")"    { return OP_RIGHT_PAR; }
{ID}   { yylval.char_t = strdup(yytext); return TK_ID; }

%%

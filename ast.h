#ifndef AST_H
#define AST_H

#include <string>
 using namespace std;

typedef struct generated{
    string code;
    string place;
} generated_t;

using namespace std;

enum ExprKind {
  ADD_EXPR,
  SUB_EXPR,
  DIV_EXPR,
  MULT_EXPR,
  NUM_EXPR,
  VAR_EXPR,
};

class Expression{
public:
  virtual void generateCode()=0;
  generated_t generation;
  virtual int getKind()=0;
  virtual bool evaluate(int &c)=0;
  bool isKind(int c){ return (c == getKind()); };
};

class BinaryExpr : public Expression{
protected:
    BinaryExpr(Expression * left, Expression *right){
      this->left = left;
      this->right = right;
    }
public:
    Expression * right;
    Expression * left;
};

class AddExpr: public BinaryExpr {
public:
  AddExpr(Expression * left, Expression *right): BinaryExpr(left, right){}
  void generateCode();
  bool evaluate(int &c);
  int getKind() { return ADD_EXPR; }
};

class SubExpr: public BinaryExpr {
public:
  SubExpr(Expression * left, Expression *right): BinaryExpr(left, right){}
  void generateCode();
  bool evaluate(int &c);
  int getKind() { return SUB_EXPR; }
};

class MulExpr: public BinaryExpr {
public:
  MulExpr(Expression * left, Expression *right): BinaryExpr(left, right){}
  void generateCode();
  bool evaluate(int &c);
  int getKind() { return MULT_EXPR; }
};

class DivExpr: public BinaryExpr {
public:
  DivExpr(Expression * left, Expression *right): BinaryExpr(left, right){}
  void generateCode();
  bool evaluate(int &c);
  int getKind() { return DIV_EXPR; }
};

class NumberExpr : public Expression{
public:
  NumberExpr(string num){
    this->num = num;
  }
  void generateCode();
  bool evaluate(int &c);
  int getKind() { return NUM_EXPR; }
  string num;

};

class VarExpr :public Expression{
public:
  VarExpr(string id){
    this->id = id;
  }
  void generateCode();
  bool evaluate(int &c);
  int getKind() { return VAR_EXPR; }
  string id;
};


#endif

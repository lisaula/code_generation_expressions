TARGET = sample
LEXER_SRC = lexer.cpp
PARSER_SRC = parser.cpp
SOURCE_FILES = $(PARSER_SRC) $(LEXER_SRC) ast.cpp main.cpp
OBJ_FILES = ${SOURCE_FILES:.cpp = .o}
.PHONY: clean


$(TARGET) : $(SOURCE_FILES)
	g++ -std=c++11 -o $@ $^

$(PARSER_SRC): parser.y
	bison -v --defines=tokens.h -o $@ $<

$(LEXER_SRC) : lexer.l
	flex -o $@ $<

%.o: %.cpp tokens.h tools.h
	g++ -std=c++11 -c -o $@ $<

run: $(TARGET)
	./$(TARGET) < input.txt

clean:
	rm -f $(TARGET)
	rm -f $(LEXER_SRC)
	rm -f $(PARSER_SRC)
	rm -f tokens.h
	rm -f parser.output

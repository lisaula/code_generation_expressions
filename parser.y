%code requires {
  #include "string.h"
  #include "string"
  #include "ast.h"
}


%{
#include <stdio.h>
int yylex();
#define YYERROR_VERBOSE 1

extern int yylineno;
int yyerror(const char* message){
  printf("Line %d: %s\n",yylineno,message );
}

%}

%union{
  Expression *Expression_t;
  char* char_t;
}

%type <Expression_t> E T F

%token OP_ADD "+"
%token OP_MINUS "-"
%token OP_MUL "*"
%token OP_DIV "/"
%token OP_LEFT_PAR "("
%token OP_RIGHT_PAR ")"
%token<char_t> TK_NUMBER TK_ID

%%
input: E { $1->generateCode();printf("%s\n", $1->generation.code.c_str()); }

E: E "+" T { $$ = new AddExpr($1,$3); }
  |E "-" T { $$ = new SubExpr($1,$3); }
  | T { $$ = $1; }
;

T: T "*" F { $$ = new MulExpr($1,$3); }
  | T "/" F { $$ = new DivExpr($1,$3); }
  | F { $$ = $1; }
;

F: TK_NUMBER { $$ = new NumberExpr($1); delete $1; }
  | TK_ID   { $$ = new VarExpr($1); delete $1; }
  | "(" E ")" { $$ = $2; }
;

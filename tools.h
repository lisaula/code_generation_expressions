#ifndef TOOLS_H
#define TOOLS_H
#include <map>
#include <string>
#include <stdio.h>
typedef unsigned char byte;
//typedef bitset<1> bit;
map<string, byte> temps = {
  {"$t0", 0}, {"$t5", 0},
  {"$t1", 0}, {"$t6", 0},
  {"$t2", 0}, {"$t7", 0},
  {"$t3", 0}, {"$t8", 0},
  {"$t4", 0}, {"$t9", 0},
};
/*void mapInitialization();
void mapInitialization(){
  temps["$t0"] = 0;temps["$t5"] = 0;
  temps["$t1"] = 0;temps["$t6"] = 0;
  temps["$t2"] = 0;temps["$t7"] = 0;
  temps["$t3"] = 0;temps["$t8"] = 0;
  temps["$t4"] = 0;temps["$t9"] = 0;
}*/

string newTemp(){
  map<string, byte>::iterator it = temps.begin();
  while(it != temps.end()){
    if(!it->second){
      it->second = 1;
      return it->first;
    }
    it++;
  }

  return "out of temporal registers";
}

string itoa(int c){
  char buffer[10];
  sprintf(buffer, "%d", c);
  return string(buffer);
}

void releaseTemp(string temp){
  temps[temp] = 0;
}

#endif

#include "ast.h"
#include "tools.h"
void VarExpr::generateCode(){
  generation.place = newTemp();
  generation.code = "lw "+generation.place+", "+id;
}

bool VarExpr::evaluate(int &c){
  return false;
}

void NumberExpr::generateCode(){
  generation.place = newTemp();
  generation.code = "li "+generation.place+", "+num;
}

bool NumberExpr::evaluate(int &c){
  c = atoi(this->num.c_str());
  return true;
}

bool AddExpr::evaluate(int &c){
  int l, r;
  if(left->evaluate(l) && right->evaluate(r)){
    c = l + r;
    return true;
  }
  return false;
}

void AddExpr::generateCode(){
  int c;
  if(evaluate(c)){
    generation.place = newTemp();
    generation.code = "lw "+generation.place+", "+itoa(c);
  }else if(left->evaluate(c)){
    // printf("entro 1ro\n");
    right->generateCode();
    string rightCode = right->generation.code;
    generation.code = rightCode + "\n";
    releaseTemp(right->generation.place);
    generation.place = newTemp();
    generation.code += "addi "+generation.place+", "+right->generation.place+", "+itoa(c);
  } else if(right->evaluate(c)){
    // printf("entro 2do\n");
    left->generateCode();
    string leftCode = left->generation.code;
    //printf("left: %s, type %d\n",leftCode.c_str(), left->getKind() );
    generation.code = leftCode + "\n";
    releaseTemp(left->generation.place);
    generation.place = newTemp();
    generation.code += "addi "+generation.place+", "+left->generation.place+", "+itoa(c);
  }else {
    string leftCode, rightCode;
    right->generateCode();
    rightCode = right->generation.code;
    left->generateCode();
    leftCode = left->generation.code;
    generation.code = leftCode + "\n" + rightCode;
    releaseTemp(left->generation.place);
    releaseTemp(right->generation.place);
    generation.place = newTemp();
    generation.code += "\nadd "+generation.place+", "+left->generation.place+", "+right->generation.place;
  }
}

bool SubExpr::evaluate(int &c){
  int l, r;
  if(left->evaluate(l) && right->evaluate(r)){
    c = l - r;
    return true;
  }
  return false;
}

void SubExpr::generateCode(){
  int c;
  if(evaluate(c)){
    generation.place = newTemp();
    generation.code = "lw "+generation.place+", "+itoa(c);
  }else if(right->evaluate(c)){
    left->generateCode();
    string leftCode = left->generation.code;
    generation.code = leftCode + "\n";
    releaseTemp(left->generation.place);
    generation.place = newTemp();
    generation.code += "addi "+generation.place+", "+left->generation.place+", -"+itoa(c);
  } else {
    left->generateCode();
    right->generateCode();
    string leftCode = left->generation.code;
    string rightCode = right->generation.code;
    generation.code = leftCode + "\n" + rightCode;
    releaseTemp(left->generation.place);
    releaseTemp(right->generation.place);
    generation.place = newTemp();
    generation.code += "\nsub "+generation.place+", "+left->generation.place+", "+right->generation.place;
  }
}

bool MulExpr::evaluate(int &c){
  int l, r;
  if(left->evaluate(l) && right->evaluate(r)){
    c = l * r;
    return true;
  }
  return false;
}

void MulExpr::generateCode(){
  int c;
  if(evaluate(c)){
    generation.place = newTemp();
    generation.code = "lw "+generation.place+", "+itoa(c);
  }else{
    left->generateCode();
    right->generateCode();
    string leftCode = left->generation.code;
    string rightCode = right->generation.code;
    generation.code = leftCode + "\n" + rightCode;
    releaseTemp(left->generation.place);
    releaseTemp(right->generation.place);
    generation.place = newTemp();
    generation.code += "\nmult "+left->generation.place+", "+right->generation.place;
    generation.code += "\nmflo "+generation.place;
  }
}

bool DivExpr::evaluate(int &c){
  int l, r;
  if(left->evaluate(l) && right->evaluate(r)){
    c = l / r;
    return true;
  }
  return false;
}

void DivExpr::generateCode(){
  int c;
  if(evaluate(c)){
    generation.place = newTemp();
    generation.code = "lw "+generation.place+", "+itoa(c);
  } else {
    left->generateCode();
    right->generateCode();
    string leftCode = left->generation.code;
    string rightCode = right->generation.code;
    generation.code = leftCode + "\n" + rightCode;
    releaseTemp(left->generation.place);
    releaseTemp(right->generation.place);
    generation.place = newTemp();
    generation.code += "\ndiv "+left->generation.place+", "+right->generation.place;
    generation.code += "\nmflo "+generation.place;
  }
}
